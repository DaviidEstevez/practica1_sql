package activitat1.logica;

import activitat1.dades.*;
import java.util.Date;

public class Tiquet {

    private int id;
    private String descripcio;
    private Date dataInici, dataFinal;
    private boolean finalitzat;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public Date getDataInici() {
        return dataInici;
    }

    public void setDataInici(Date dataInici) {
        this.dataInici = dataInici;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public boolean isFinalitzat() {
        return finalitzat;
    }

    public void setFinalitzat(boolean finalitzat) {
        this.finalitzat = finalitzat;
    }

    @Override
    public String toString() {
        return "Tiquet{" + "id=" + id + ", descripcio=" + descripcio + ","
                + " dataInici=" + dataInici + ", dataFinal=" + dataFinal + ", finalitzat=" + finalitzat + '}';
    }
    
    

}
