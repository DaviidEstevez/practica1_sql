package activitat1.dades.impl;

import activitat1.dades.ConnectionFactory;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.pool.OracleDataSource;


public class ConnectionFactoryImpl implements ConnectionFactory {
    
    private String URL = "jdbc:oracle:thin:@ieslaferreria.xtec.cat:8081:INSLAFERRERI";
    private String USER = "DANAYA";
    private String PASSW = "1234";
    
    @Override
    public Connection getConnection() throws SQLException {
        
        Connection con = null;
        try {
            
            OracleDataSource ods = dataSourceConfig();
         
            con = ods.getConnection();
            
            System.out.println("Connection OK!");
            
        } catch (SQLException ex) {
            System.out.println("Error coneccion fallida. "+ ex);
        }
        
         return con;
    }
    
    public OracleDataSource dataSourceConfig() {
        OracleDataSource ods = null;
        try {
            ods = new OracleDataSource();
            ods.setURL(URL);
            ods.setUser(USER);
            ods.setPassword(PASSW);
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionFactoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ods;
    }

}
