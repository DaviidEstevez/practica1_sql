package activitat1.dades.impl;

import activitat1.dades.ConnectionFactory;
import activitat1.dades.TiquetDAO;
import activitat1.dades.TiquetTO;
import activitat1.dades.exceptions.DateException;
import activitat1.dades.exceptions.NotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TiquetDAOImpl implements TiquetDAO {

    private ConnectionFactory cf;
    private Connection con;

    public TiquetDAOImpl(ConnectionFactory cf) {
        this.cf = cf;
        try {
            con = cf.getConnection();
        } catch (SQLException ex) {
            System.out.println("Conection failed "+ ex);
        }
    }

    @Override
    public void clear() {
        String sentencia = "TRUNCATE TABLE Tiquets ";
       
        try {
            
            Statement statement = con.createStatement();
            statement.execute(sentencia);
            
        } catch (SQLException ex) {
            System.out.println("CLEAR no funciono! " + ex);
        }
    }

    
    @Override
    public TiquetTO find(int id) throws NotFoundException {
        TiquetTO newTiquet = null;
       
        String sentencia = "SELECT id_tiquet, descripcio, datainici, datafinal, finalitzat FROM Tiquets WHERE id_tiquet = ?";

        try {

            PreparedStatement statement = con.prepareStatement(sentencia);
            statement.setInt(1, id);
            ResultSet resultado = statement.executeQuery();
            boolean finalitzat = false;
            if (resultado.next()) {
                if (resultado.getInt(5) == 1) {
                    finalitzat = true;
                }
                newTiquet = new TiquetTO(resultado.getInt(1), resultado.getString(2), resultado.getDate(3), resultado.getDate(4), finalitzat);
            }

        } catch (SQLException ex) {
            System.out.println("FIND no funciono "+ ex);
        }
        return newTiquet;
    }

    @Override
    public void delete(int id) throws NotFoundException {
        
        String sentencia = "DELETE FROM Tiquets WHERE id_tiquet = ? ";
        try {
            
            PreparedStatement statement = con.prepareStatement(sentencia);
            statement.setInt(1, id);
            statement.executeUpdate();  
            
        } catch (SQLException ex) {
            System.out.println("DELETE no funciono " + ex);
        }

    }

    @Override
    public void create(TiquetTO tiquet) {
       String sentencia = "INSERT INTO Tiquets(id_tiquet, descripcio, dataInici, dataFinal, finalitzat) VALUES (?, ?, ?, ?, ?) ";
        System.out.println(sentencia +"\n"+tiquet.toString());
        try {
           int finalitzat = 0;
            if(tiquet.isFinalitzat()){
                finalitzat = 1;
            }
            PreparedStatement statement = con.prepareStatement(sentencia);
            statement.setInt(1, tiquet.getId());
            statement.setString(2, tiquet.getDescripcio());
            statement.setDate(3, (java.sql.Date) tiquet.getDataInici());
            statement.setDate(4, (java.sql.Date) tiquet.getDataFinal());
            statement.setInt(5, finalitzat);
            
            statement.executeUpdate();  
            
        } catch (SQLException ex) {
            System.out.println("INSERT no funciono " + ex);
        }

    }

    @Override
    public void update(TiquetTO tiquet) throws NotFoundException {
        String sentencia = "UPDATE Tiquets SET descripcio = ?, dataInici = ?, dataFinal = ?, finalitzat = ? WHERE id_tiquet = ?";
        int finalitzat = 0;
        if(tiquet.isFinalitzat()){
            finalitzat = 1;
        }
        try {
            
            PreparedStatement statement = con.prepareStatement(sentencia);
            statement.setString(1, tiquet.getDescripcio());
            statement.setDate(2, (java.sql.Date) tiquet.getDataInici());
            statement.setDate(3, (java.sql.Date) tiquet.getDataFinal());
            statement.setInt(4, finalitzat);
            statement.setInt(5, tiquet.getId());
            statement.executeUpdate();  
            
        } catch (SQLException ex) {
            System.out.println("UPDATE no funciono " + ex);
        }

    }

    @Override
    public List<TiquetTO> findAll() {
        List<TiquetTO> tiquets = new ArrayList<TiquetTO>();
        
        String sentencia = "SELECT id_tiquet, descripcio, datainici, datafinal, finalitzat FROM Tiquets";
        
        try {
            
            Statement statement = con.createStatement();
           
            ResultSet resultado = statement.executeQuery(sentencia);
            
            while(resultado.next()){ 
                boolean finalitzat = false;
                if(resultado.getInt(5) == '1'){
                    finalitzat = true;
                }
                TiquetTO tiquet = new TiquetTO(resultado.getInt(1), resultado.getString(2), resultado.getDate(3), resultado.getDate(4), finalitzat);
                tiquets.add(tiquet);
            }
            
        } catch (SQLException ex) {
            System.out.println("FIND no funciono "+ ex);
        }
        return tiquets;
    }

    @Override
    public List<TiquetTO> finalitzats() {
        List<TiquetTO> tiquets = new ArrayList<TiquetTO>();;
        
        String sentencia = "SELECT id_tiquet, descripcio, datainici, datafinal, finalitzat FROM Tiquets WHERE finalitzat = 0";
        
        try {
            
            Statement statement = con.createStatement();
           
            ResultSet resultado = statement.executeQuery(sentencia);
            
            while(resultado.next()){ 
                boolean finalitzat = false;
                if(resultado.getInt(5) == '1'){
                    finalitzat = true;
                }
                TiquetTO tiquet = new TiquetTO(resultado.getInt(1), resultado.getString(2), resultado.getDate(3), resultado.getDate(4), finalitzat);
                tiquets.add(tiquet);
            }
            
        } catch (SQLException ex) {
            System.out.println("FIND no funciono "+ ex);
        }
        return tiquets;
    }

    @Override
    public List<TiquetTO> datesRang(Date dataInici, Date dataFi) throws DateException {
        List<TiquetTO> tiquets = new ArrayList<TiquetTO>();;
        
        String sentencia = "SELECT id_tiquet, descripcio, datainici, datafinal, finalitzat FROM Tiquets WHERE dataInici = ? AND dataFinal = ?";
        
        try {
            
            PreparedStatement statement = con.prepareStatement(sentencia);
            
            statement.setDate(1, (java.sql.Date) dataInici);
            statement.setDate(2, (java.sql.Date) dataFi);
           
            ResultSet resultado = statement.executeQuery();
            while(resultado.next()){ 
                
                boolean finalitzat = false;
                if(resultado.getInt(5) == '1'){
                    finalitzat = true;
                }
                TiquetTO tiquet = new TiquetTO(resultado.getInt(1), resultado.getString(2), resultado.getDate(3), resultado.getDate(4), finalitzat);
                tiquets.add(tiquet);
            }
            
        } catch (SQLException ex) {
            System.out.println("DATESRANG no funciono "+ ex);
        }
        return tiquets;
    }
    
    
    public void createTableTiquet() {
        String sentencia = "CREATE TABLE Tiquets ( id_Tiquet INT PRIMARY KEY, descripcio VARCHAR2(250), dataInici DATE, dataFinal DATE, finalitzat INT CHECK (finalitzat IN (0,1) ) )";
        System.out.println(sentencia);
        try {
            
            Statement statement = con.createStatement();
            statement.execute(sentencia);
            
        } catch (SQLException ex) {
            System.out.println("Error tabla ya existe! " + ex);
        }
    }

}
