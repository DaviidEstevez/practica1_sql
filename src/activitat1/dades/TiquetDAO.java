package activitat1.dades;

import activitat1.dades.exceptions.DateException;
import activitat1.dades.exceptions.NotFoundException;
import java.util.Date;
import java.util.List;

public interface TiquetDAO {

    /**
     * Esborrar el contingut de la taula
     */
    void clear();

    /**
     * Trobar un tiquet per id
     *
     * @param id
     * @return
     * @throws NotFoundException
     */
    TiquetTO find(int id) throws NotFoundException;

    /**
     * Esborrar un tiquet per id
     *
     * @param id
     * @throws NotFoundException
     */
    void delete(int id) throws NotFoundException;

    /**
     * crear un tiquet
     *
     * @param tiquet
     */
    void create(TiquetTO tiquet);

    /**
     * Modificar un tiquet
     *
     * @param tiquet
     * @throws NotFoundException
     */
    void update(TiquetTO tiquet) throws NotFoundException;

    /**
     * Trobar tots els tiquets
     *
     * @return
     */
    List<TiquetTO> findAll();

    /**
     * Torna tots els tiquets finalitzats
     *
     * @return
     */
    List<TiquetTO> finalitzats();

    /**
     * Torna els tiquets creats entre les dues dates, si dataInici és posterior
     * a dataFies llançarà una DateException
     *
     * @param desde
     * @param fins
     * @return
     * @throws DateException
     */
    List<TiquetTO> datesRang(Date dataInici, Date dataFi) throws DateException;

}
