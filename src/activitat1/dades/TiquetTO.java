package activitat1.dades;

import java.util.Date;

public class TiquetTO {

    public int id;
    public String descripcio;
    public Date dataInici, dataFinal;
    public boolean finalitzat;

    public TiquetTO() {
    }
    
    public TiquetTO(int id, String descripcio, Date dataInici, Date dataFinal, boolean finalitzat) {
        this.id = id;
        this.descripcio = descripcio;
        this.dataInici = dataInici;
        this.dataFinal = dataFinal;
        this.finalitzat = finalitzat;
    }
    
    @Override
    public String toString() {
        return "Tiquet{" + "id=" + id + ", descripcio=" + descripcio + ","
                + " dataInici=" + dataInici + ", dataFinal=" + dataFinal + ", finalitzat=" + finalitzat + '}';
    }
    
        public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public Date getDataInici() {
        return dataInici;
    }

    public void setDataInici(Date dataInici) {
        this.dataInici = dataInici;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public boolean isFinalitzat() {
        return finalitzat;
    }

    public void setFinalitzat(boolean finalitzat) {
        this.finalitzat = finalitzat;
    }

}
