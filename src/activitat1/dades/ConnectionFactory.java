package activitat1.dades;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionFactory {

    /**
     * Torna un Connection
     *
     * @return
     * @throws SQLException
     */
    Connection getConnection() throws SQLException;
}
