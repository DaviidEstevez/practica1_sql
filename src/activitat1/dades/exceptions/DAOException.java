package activitat1.dades.exceptions;

public class DAOException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public DAOException(String message) {
		super(message);
	}

	public DAOException(Throwable t) {
		super(t);
	}
	
	public DAOException(String message, Throwable t) {
		super(message, t);
	}

}
