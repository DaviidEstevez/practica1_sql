package activitat1.dades.exceptions;

public class NotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public NotFoundException(String message) {
		super(message);
	}
	
	public NotFoundException(Throwable t) {
		super(t);
	}
	
	public NotFoundException(String message, Throwable t) {
		super(message, t);
	}
	
}
