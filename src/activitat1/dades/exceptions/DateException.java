package activitat1.dades.exceptions;


public class DateException  extends Exception {

	private static final long serialVersionUID = 1L;

	public DateException(String message) {
		super(message);
	}
	
	public DateException(Throwable t) {
		super(t);
	}
	
	public DateException(String message, Throwable t) {
		super(message, t);
	}
}
	
