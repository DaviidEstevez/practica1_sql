package activitat1.dades;

import activitat1.dades.impl.ConnectionFactoryImpl;
import activitat1.dades.impl.TiquetDAOImpl;
import java.sql.SQLException;
import java.util.logging.Logger;

public class DAOFactory {

    private static final Logger LOGGER = Logger.getLogger(DAOFactory.class.getName());

    private static DAOFactory instance;
    private ConnectionFactory connFactory;


    private DAOFactory() {
        connFactory = new ConnectionFactoryImpl();
        
    }

    public static DAOFactory getInstance() {

        if (instance == null) {
            instance = new DAOFactory();
        }

        return instance;
    }

    public TiquetDAO getTiquetDAO() throws SQLException {
        return new TiquetDAOImpl(connFactory);

    }
}
