package activitat1.test;

import activitat1.dades.TiquetTO;
import activitat1.dades.exceptions.DateException;
import activitat1.dades.exceptions.NotFoundException;
import activitat1.dades.impl.ConnectionFactoryImpl;
import activitat1.dades.impl.TiquetDAOImpl;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;


public class TestTiquetDAO {

    private static TiquetDAOImpl tdao;
    private static java.sql.Date dataIniciSql;
    private static java.sql.Date dataFinSql;
    
    public static void main(String[] args) {
        tdao = new TiquetDAOImpl(new ConnectionFactoryImpl());
        boolean salir = true;
        Scanner lec = new Scanner(System.in);
        do{
            
            System.out.println("Elige alguna pocion \n"
                    + "0. crearTabla\n"
                    + "1. crearTiquet\n"
                    + "2. buscarTiquet\n"
                    + "3. esborrarTiquet\n"
                    + "4. modificarTiquet\n"
                    + "5. llistarTotsElsTiquets\n"
                    + "6. llistarFinalitzats\n"
                    + "7. llistarTiquetsEntreRangDates\n"
                    + "8. clearTabla\n"
                    + "x. (SALIR)\n");
               switch(lec.nextLine()){
                   case "0":
                       crearTabla();
                    break;
                    case "1":
                       crearTiquet();
                    break;
                    case "2":
                        cercarTiquet();
                    break;
                    case "3":
                        esborrarTiquet();
                    break;
                    case "4":
                        modificarTiquet();
                    break;
                    case "5":
                        llistarTotsElsTiquets();
                    break;
                    case "6":
                        llistarFinalitzats();
                    break;
                    case "7":
                        llistarTiquetsEntreRangDates();
                    break;
                    case "8":
                        clearTabla();
                    break;
                    case "x":
                        salir = false;
                    break;
               }
            
        }while(salir);
    }
    
    static void crearTiquet(){
        Date dataInici;
        dataInici= new java.util.Date(System.currentTimeMillis());
        java.sql.Date dataIniciSql = new java.sql.Date(dataInici.getTime());
        TestTiquetDAO.dataIniciSql = dataIniciSql;
        TestTiquetDAO.dataFinSql = null;
       tdao.create(new TiquetTO(1, "Primer tiquet", dataIniciSql, null, false));
    }
    
    static void crearTabla(){
        tdao.createTableTiquet();
    }
    
    static void clearTabla(){
        tdao.clear();
    }
    static void cercarTiquet(){
        try {
            System.out.println(tdao.find(1));
        } catch (NotFoundException ex) {
            Logger.getLogger(TestTiquetDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static void esborrarTiquet(){
        try {
            tdao.delete(1);
        } catch (NotFoundException ex) {
            Logger.getLogger(TestTiquetDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static void modificarTiquet(){
        Date dataInici, dataFin;
        dataInici= new java.util.Date(System.currentTimeMillis()-1000000000);
        java.sql.Date dataIniciSql = new java.sql.Date(dataInici.getTime());
        dataFin =new java.util.Date(System.currentTimeMillis()+1000000000);
        java.sql.Date dataFinSql = new java.sql.Date(dataFin.getTime());
        
        TestTiquetDAO.dataIniciSql = dataIniciSql;
        TestTiquetDAO.dataFinSql = dataFinSql;
        try {
            tdao.update(new TiquetTO(1,"Primer tiquet Update", dataIniciSql, dataFinSql, true));
        } catch (NotFoundException ex) {
            Logger.getLogger(TestTiquetDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static void llistarTotsElsTiquets(){        
        System.out.println(tdao.findAll());
    }
    static void llistarFinalitzats(){
        System.out.println(tdao.finalitzats());
    }
    
    static void llistarTiquetsEntreRangDates(){
        try {
            System.out.println(tdao.datesRang(dataIniciSql, dataFinSql));
        } catch (DateException ex) {
            Logger.getLogger(TestTiquetDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
          
}
